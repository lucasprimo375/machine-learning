import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random
import math

arquivoDados = open("ex2data1.txt", "r")
dados = arquivoDados.read()
dados = dados.split("\n");

del dados[len(dados)-1]

#dados = dados[:10]

d = len(dados[0].split(","))

n = len(dados)
x = []
y = []

for i in range(0, n):
    y.append(float(dados[i].split(",")[d-1]))


for i in range(0, n):

    aux = [1]
    for j in range(0, d-1):
        aux.append(float(dados[i].split(",")[j]))

    x.append(aux)

y = np.array(y);

for i in range(0, d-1):
    x[i] = np.array(x[i])


w = []
for i in range(0, d):
    w.append(random.randint(-n,n)/n + 0.0)

w = np.array(w)

epocas = 1000
alfa = 0.001
for i in range(0, epocas):
    s = []
    for j in range(0, d):
        s.append(0.0)

    s = np.array(s)

    for j in range(0, n):
        s = s + np.multiply(x[j], y[j] - 1/(1 + math.exp(-np.dot(w, x[j]))))

    w = w + np.multiply(s, alfa/n)

zBarra = []
for i in range(0, n):
    if(1/(1 + math.exp(-np.dot(w, x[i]))) >= 0.5):
        print(1)
        zBarra.append(1)
    else:
    	zBarra.append(0)
    	print(0)

count = 0
for i in range(0, n):
	if(y[i] == zBarra[i]):
		count+=1

print(count/n)


for i in range(0, n):
    x[i] = np.delete(x[i], 0)

x1 = []
x2 = []
for i in range(0, n):
    x1.append(x[i][0])
    x2.append(x[i][1])

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.scatter(x1, x2, y);
ax.scatter(x1, x2, zBarra);

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

ax.legend()
plt.show()