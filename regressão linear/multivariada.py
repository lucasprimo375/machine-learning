import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

arquivoDados = open("ex1data2.txt", "r")
dados = arquivoDados.read()
dados = dados.split("\n");

del dados[len(dados)-1]

d = len(dados[0].split(" "))

n = len(dados)
x = []
y = []

for i in range(0, n):
	y.append(float(dados[i].split(" ")[d-1]))

for i in range(0, n):

	aux = [1]
	for j in range(0, d-1):
		aux.append(float(dados[i].split(" ")[j]))

	x.append(aux)

y = np.array(y);

for i in range(0, d-1):
	x[i] = np.array(x[i])

w = []
for i in range(0, d):
	w.append(0.0)

w = np.array(w);

epocas = 1000
alfa = 0.001
for i in range(0, epocas):
	for j in range(0, n):
		w = w + np.dot((alfa/n)*(y[j] - np.dot(w, x[j]) - w), x[j])

zBarra = []
for i in range(0, n):
	zBarra.append(np.dot(w, x[i]))

for i in range(0, n):
	x[i] = np.delete(x[i], 0)

x1 = []
x2 = []
for i in range(0, n):
	x1.append(x[i][0])
	x2.append(x[i][1])

fig = plt.figure()
ax = fig.gca(projection='3d')

ax.scatter(x1, x2, y);
ax.scatter(x1, x2, zBarra);

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

ax.legend()
plt.show()