# regressao linear univariada

from random import randint
import numpy as np

arquivoDados = open("ex1data1.txt", "r")
dados = arquivoDados.read()
dados = dados.split("\n");

del dados[len(dados)-1]

d = len(dados[0].split(","))

n = len(dados)
x = []
y = []
for i in range(0,n):
	x.append(float(dados[i].split(",")[0]))
	y.append(float(dados[i].split(",")[1]))

x = np.array(x);
y = np.array(y);

w = []
for i in range(0, d):
	w.append(randint(-n,n)/n + 0.0)

w = np.array(w);

import matplotlib.pyplot as plt

epocas = 1000
alfa = 0.001
for i in range(0, epocas):
	for j in range(0, n):
		w[0] = w[0] + (alfa/n)*(y[j] - w[1]*x[j] - w[0])
		w[1] = w[1] + (alfa/n)*(y[j] - w[1]*x[j] - w[0])*(x[j])

yBarra = []
for i in range(0, n):
	yBarra.append(w[1]*x[i] + w[0])

plt.title("Dados originais com a funcao")
plt.plot(x, y, "bo", x, yBarra, "rx")
plt.show()
